LUA ?= lua5.3
LUAV ?= 5.3
LUA_SHARE ?= /usr/share/lua/$(LUAV)
STATIC_LUA_LIB ?= /usr/lib/x86_64-linux-gnu/liblua5.3.a
#This path should work for Debian based systems
#On Alpine the correct lib path is /usr/lib/liblua-5.3.so.0.0.0
LUA_INCLUDE_DIR ?= /usr/include/lua5.3
DESTDIR ?= /usr/local/bin

compile-fennel:
	chmod u+x ./esperbuild/esper
	cd ./esperbuild && ./esper fennel-060-bin.esper

compile-lua:
	fennel --compile --require-as-include src/fa.fnl > src/fa.lua
	sed -i '1 i\-- Author: Jesse Laprade <jesselaprade@gmail.com> | License: AGPLv3' src/fa.lua
	sed -i '1 i\#!/usr/bin/$(LUA)' src/fa.lua

install-lua:
	install -Dm755 ./src/fa.lua -D $(DESTDIR)/fa

compile-bin:
	cd ./src/ && fennel --compile-binary fa.fnl fa-bin $(STATIC_LUA_LIB) $(LUA_INCLUDE_DIR)

install-bin:
	install -Dm755 ./src/fa-bin -D $(DESTDIR)/fa
