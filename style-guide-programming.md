# fa's programming style guide

This document outlines preferred programming styles when contributing to the fa project.

This document is mainly for my friend [Will Sinatra](http://lambdacreate.com/)
who knows I'm not actually as picky as I seem in this guide.

# To discuss

* Do we want to use a `?` suffix (like Scheme) or `-p` suffix (like
  common lisp) when defining boolean-returning functions? (w . yes to function? style)
* Do we agree with my `let` statement naming and indentation conventions? (w . yes, it's more legible)
* Do we agree with my `match` indentation conventions? (w. yes, same as match)
* Do we want to use `->` to mean `to` in conversion functions, such as
  `table->file` or `tbl->file`, or do we want to use `to`, such as
  `table-to-file` or `tbl-to-file`? (w. tbl->file is arguably more fennel, so that's my vote)

<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->
# Table of Contents

- [Naming](#naming)
    - [Kebab case](#kebab-case)
    - [Shortening names](#shortening-names)
    - [Categorizing names](#categorizing-names)
    - [Naming locals](#naming-locals)
    - [Naming functions](#naming-functions)
    - [Naming parameters](#naming-parameters)
    - [Naming let bindings](#naming-let-bindings)
- [Indentation](#indentation)
    - [Text editor configuration](#text-editor-configuration)
    - [let indentation](#let-indentation)
    - [match indentation](#match-indentation)
- [Abbreviations](#abbreviations)

<!-- markdown-toc end -->

# Naming

This section describes naming conventions related to the shortening of
names, locals, functions, and parameters.

This section consists of the following subsections:

* [Shortening names](#shortening-names)
* [Naming locals](#naming-locals)
* [Naming functions](#naming-functions)
* [Naming parameters](#naming-parameters)
* [Naming let bindings](#naming-let-bindings)

## Kebab case

Prefer kebab case over other cases.

### Preferred example(s)

* `user-str`
* `arg-tbl`
* `local-network`

### Undesired example(s)

* `userStr`
* `Argtbl`
* `LocalNetwork`

## Shortening names

Prefer guessable, three-letter names if you are shortening a name.

### Preferred example(s)

* `arg-table` can be shorted to `arg-tbl`
* `arg-str` can be shorted to `arg-str`
* `arg-num` can be shorted to `arg-num`

### Undesired example(s)

* `arg-table` can be shorted to `arg-t`
* `arg-str` can be shorted to `arg-s`
* `arg-num` can be shorted to `arg-n`

## Categorizing names

Prefer to prefix names with a category if they fall under the same
category.

### Preferred example(s)

```
(local cmd-add "add")
(local cmd-rm "rm")
(local cmd-ls "ls")
```

### Undesired example(s)

```
(local add-cmd "add")
(local rm-cmd "rm")
(local ls-cmd "ls")
```

(Yes, I did this in the original code, and I need to fix it haha!)

## Naming locals

Prefer verbose, descriptive local names, unless you can shorten the
name according to the [Shortening names](#shortening-names) section.

### Preferred example(s)

* `number-found`
* `number-received`
* `num-found`
* `num-received`

### Undesired example(s)

* `n-found`
* `n-received`

## Naming functions

Prefer verbose, descriptive function names, unless you can shorten the
name according to the [Shortening names](#shortening-names) section.

If the function returns `true` or `false`, add a **To discuss. See the
[To discuss](#to-discuss) section above.**

### Preferred example(s)

* `stringify-user-id`
* `user-id-to-string`
* `user-id->string`
* `user-id->str`
* `get-user-id`

### Undesired example(s)

* `s-user-id`
* `user-id-s`
* `user-id-to-s`

## Naming parameters

Prefer verbose, descriptive parameter names, unless you can shorten the name
according to the [Shortening names](#shortening-names) section.

Append the parameter type at the end of the name, shortening to a
three-letter type, if possible.

### Preferred example(s)

```
(fn greet-user [name-str]
  (print (.. "Hello, " name-str)))
```

### Undesired example(s)

```
(fn grt-u [n]
  (print (.. "Hello, " n)))
```

## Naming let bindings

Prefer verbose, descriptive parameter names, unless you can shorten the name
according to the [Shortening names](#shortening-names) section.

Append the parameter type at the end of the name, shortening to a
three-letter type, if possible.

### Preferred example(s)

```
(fn greet-two-numbers [num-1 num-2]
  (let [num-1-str     (tostring num-1)
        num-2-str     (tostrign num-2)
        nums-combined (.. num-1-str num-2-str)]
    (print (.. "Hello, " nums-combined))))

```

### Undesired example(s)

```
(fn greet-two-numbers [num-1 num-2]
  (let [n-1-str (tostring num-1)
        n-2-str (tostrign num-2)
        n-combined (.. n-1-str n-2-str)]
    (print (.. "Hello, " n-combined))))

```

# Indentation

This section describes preferred indentation conventions.

This section consists of the following subsections:

* [Text editor configuration](#text-editor-configuration)
* [let indentation](#let-indentation)
* [match indentation](#match-indentation)

## Text editor configuration

Prefer two-spaced indentation, instead of tabs.

## let indentation

Indent let bindings so they line up vertically.

### Preferred example(s)

```
(fn greet-two-numbers [num-1 num-2]
  (let [num-1-str     (tostring num-1)
        num-2-str     (tostrign num-2)
        nums-combined (.. num-1-str num-2-str)]
    (print (.. "Hello, " nums-combined))))

```

### Undesired example(s)

```
(fn greet-two-numbers [num-1 num-2]
  (let [num-1-str (tostring num-1)
        num-2-str (tostrign num-2)
        nums-combined (.. num-1-str num-2-str)]
    (print (.. "Hello, " nums-combined))))

```

## match indentation

Indent match patterns and result functions so they line up vertically.

If the pattern is too long, move the result function one line below
the pattern.

### Preferred example(s)

```
(match arg-tbl
  [add-cmd date-str event-str nil]
  (add date-str event-str)
  [help-cmd nil]        (help)
  [init-cmd nil]        (init)
  [rm-cmd date-str nil] (rm date-str))

```

### Undesired example(s)

```
(match arg-tbl
  [add-cmd date-str event-str nil] (add date-str event-str)
  [help-cmd nil] (help)
  [init-cmd nil] (init)
  [rm-cmd date-str nil] (rm date-str))

```

# Abbreviations

If you start using a new abbreviation, add it to the list below.

```
tbl = table
str = string
num = number

```
