# fa

A command-line agenda written in Fennel.

# Screenshot

![A screenshot of a user adding and removing agenda items in a terminal](images/screenshot.gif)

# Table of Contents

- [About this document](#about-this-document)
- [Disclaimer](#disclaimer)
- [Conventions used in this document](#conventions-used-in-this-document)
- [Platforms](#platforms)
- [Requirements](#requirements)
- [Downloading fa](#downloading-fa)
    - [Downloading the fa binary](#downloading-the-fa-binary)
        - [To download the fa binary](#to-download-the-fa-binary)
    - [Downloading the fa source code](#downloading-the-fa-source-code)
        - [To download the fa source code](#to-download-the-fa-source-code)
- [Installing fa](#installing-fa)
    - [Installing fa globally using the binary](#installing-fa-globally-using-the-binary)
        - [To install fa globally using the binary](#to-install-fa-globally-using-the-binary)
    - [Installing fa globally using the Makefile](#installing-fa-globally-using-the-makefile)
        - [To install fa globally using the Makefile](#to-install-fa-globally-using-the-makefile)
    - [Installing fa locally using the binary](#installing-fa-locally-using-the-binary)
        - [To install fa locally using the binary](#to-install-fa-locally-using-the-binary)
    - [Installing fa locally using the Makefile](#installing-fa-locally-using-the-makefile)
        - [To install fa locally using the Makefile](#to-install-fa-locally-using-the-makefile)
- [Using fa](#using-fa)
    - [Initializing fa](#initializing-fa)
        - [To initialize fa](#to-initialize-fa)
    - [Showing the help message](#showing-the-help-message)
        - [To show the help message](#to-show-the-help-message)
    - [Displaying your agenda](#displaying-your-agenda)
        - [To display your agenda](#to-display-your-agenda)
    - [Adding a date to your agenda](#adding-a-date-to-your-agenda)
        - [To add a date to your agenda](#to-add-a-date-to-your-agenda)
        - [To add the current date to your agenda](#to-add-the-current-date-to-your-agenda)
    - [Removing a date from your agenda](#removing-a-date-from-your-agenda)
        - [To remove a date from your agenda](#to-remove-a-date-from-your-agenda)
        - [To remove the current date from your agenda](#to-remove-the-current-date-from-your-agenda)
    - [Removing an event from your agenda](#removing-an-event-from-your-agenda)
        - [To remove an event from your agenda](#to-remove-an-event-from-your-agenda)
        - [To remove an event on the current date from your agenda](#to-remove-an-event-on-the-current-date-from-your-agenda)
- [List of commands](#list-of-commands)
- [Usage examples](#usage-examples)
- [Adding fa notifications to your tools](#adding-fa-notifications-to-your-tools)
    - [Adding notifications to your Bash prompt](#adding-notifications-to-your-bash-prompt)
        - [To add notifications to your Bash prompt](#to-add-notifications-to-your-bash-prompt)
    - [How fa notifications look](#how-fa-notifications-look)
- [Contributors](#contributors)
- [Todos](#todos)

# About this document

This document assumes you have basic command line skills in a Unix-like environment.

# Disclaimer

This is a hobby project I built for myself to keep track of friends'
and family members' birthdays, appointments, etc.

As mentioned in the AGPL3, I take no responsibility, and am not liable
for anything that fa deletes.

Backup anything you don't want deleted.

# Conventions used in this document

* **Note**: Notes signify additional information
* **Tip**: Tips signify an alternative procedure for completing a step
* **Warning**: Warnings signify that damage, such as data loss, may occur.
* **Example**: Examples provide a reference of how a procedure would be performed in the real world
* `inline code`: signifies package names, filenames, or commands
* `code block`: Code blocks signify file contents

# Platforms

Below is a list of platforms that fa can run on:

* Linux
  * Debian 10 (Tested)
  * Alpine Linux (To test)
* Windows (Untested)
* Windows Subsystem for Linux (Untested)
* macOS (Untested)

# Requirements

* An internet connection
* `git` (For downloading the fa source code)

# Downloading fa

Before using fa, you will need to download fa. This section will guide
you through downloading the fa binary or the fa source code.

This section consists of the following subsections:

- [Downloading the fa binary](#downloading-the-fa-binary)
- [Downloading the fa source code](#downloading-the-fa-source-code)

## Downloading the fa binary

The fa binary is a single file you can move around your system or
place in a directory on your `$PATH`, so you can use fa from anywhere
on your system.

### To download the fa binary

1. Run `wget https://git.m455.casa/m455/fa/raw/branch/main/fa && chmod u+x fa`

**Note**: You can move the `fa` executable anywhere on your system or
move it into a directory on your `$PATH`, such as `/usr/local/bin`.

## Downloading the fa source code

The fa source code can be downloaded for people who want to compile
from source. This method also makes installation easier, if you want
to run fa from anywhere on your system.

### To download the fa source code

1. `git clone https://git.m455.casa/m455/fa.git`

# Installing fa

You can install fa using the `fa` binary included in this repository
or you can compile and install fa from its source code.

This section consists of the following subsections:

- [Installing fa globally using the binary](#installing-fa-globally-using-the-binary)
- [Installing fa globally using the Makefile](#installing-fa-globally-using-the-makefile)
- [Installing fa locally using the binary](#installing-fa-locally-using-the-binary)
- [Installing fa locally using the Makefile](#installing-fa-locally-using-the-makefile)

**Note**: This section assumes you have [downloaded the fa source code](#downloading-the-fa-source-code).

## Installing fa globally using the binary

You can install fa globally by moving the binary to your
`/usr/local/bin` directory. This will require root privileges.

**Tip**: If you don't have root privileges, see one of the following sections:

- [Installing fa locally using the binary](#installing-fa-locally-using-the-binary)
- [Installing fa locally using the Makefile](#installing-fa-locally-using-the-makefile)

### To install fa globally using the binary

1. `cd fa`
2. `sudo mv fa /usr/local/bin`

## Installing fa globally using the Makefile

This section will compile fennel on your system, create a `fa-bin`
executable binary, rename `fa-bin` to `fa`, and then move the `fa`
executable binary to `/usr/local/bin`.

**Note**: This section assumes you have an internet connection.

### To install fa globally using the Makefile

1. `cd fa`
2. `sudo make compile-fennel`
3. `make compile-bin`
4. `sudo make install-bin`

## Installing fa locally using the binary

TODO: Ask will what the best approach for this might be

### To install fa locally using the binary

TODO: Ask will what the best approach for this might be

## Installing fa locally using the Makefile

TODO: Ask will what the best approach for this might be

### To install fa locally using the Makefile

TODO: Ask will what the best approach for this might be

# Using fa

This section will teach you how to use fa's commands.

This section assumes you have [installed fa](#installing-fa).

## Initializing fa

Before using fa, you will need to initialize it. This means fa will create a `~/.fa` file.

### To initialize fa

1. Run `fa init`

## Showing the help message

The help message will provide a list of available commands. This is list useful in case you forget
the name of a command or how to use a command.

### To show the help message

1. Run `fa help`

## Displaying your agenda

Displaying your agenda will allow you to view the dates you have added to your agenda.

**Note**: These dates are useful references for when you want to remove dates from your agenda. For
more information, see the [Removing a date from your agenda](#removing-a-date-from-your-agenda) topic.

### To display your agenda

1. Run `fa ls`

## Adding a date to your agenda

Adding a date to your agenda will save it to a text file to access later.

### To add a date to your agenda

1. Run `fa add mmdd "Your event text here"`

Where `mm` is two numbers representing a month and `dd` is two numbers representing a day.

**Example**: `fa add 0112 "Some event on January the 12th"`

### To add the current date to your agenda

1. Run `fa add today "Your event text here"`

## Removing a date from your agenda

When removing a date from your agenda, you can reference the date numbers beside each
agenda item when [displaying your agenda](#displaying-your-agenda). You can use these
numbers when removing a date from your agenda.

### To remove a date from your agenda

1. Run `fa rm mmdd`

Where `mm` is two numbers representing a month and `dd` is two numbers representing a day.

**Example**: `fa rm 0112`

### To remove the current date from your agenda

1. Run `fa rm today`

**Note**: You may need to run `fa ls` first to see which numbers correspond
with which date in your agenda.

## Removing an event from your agenda

Each date contains a list of events. These events can be removed
individually. This removes the need to recreate a date with multiple
events when you only need remove one event.

### To remove an event from your agenda

1. Run `fa rm mmdd n`

Where `mm` is two numbers representing a month, `dd` is two numbers representing a day, and `n` is a number that correlates to an event.

**Example**: `fa rm 1231 3`

**Note**: You may need to run `fa ls` first to see which numbers correspond
with which event in your agenda.

### To remove an event on the current date from your agenda

1. Run `fa rm today n`

Where `n` is a number that correlates to an event.

**Note**: You may need to run `fa ls` first to see which numbers correspond
with which event in your agenda.

**Example**: `fa rm today 3`

# List of commands

This section lists and describes fa's commands.

* `init` - Creates your agenda.
* `add <mmdd|today> "Quoted text"` - Adds an event to your agenda.
* `ls` - Displays a list of dates and their events from your agenda.
* `notify` - Checks if the current date matches a date in your agenda, and if it matches, then a "!" will be displayed.
* `today` - The same as the notify command, but the event text will be displayed, instead of a "!".
* `rm <mmdd|today> [<n>]` - Removes the given date from your agenda.

# Usage examples

* `fa init`
* `fa add 1231 "Sherry's birthday"`
* `fa add today "Sherry's birthday"`
* `fa ls`
* `fa notify`
* `fa today`
* `fa rm 1231`
* `fa rm 1231 3`
* `fa rm today`
* `fa rm today 3`

# Adding fa notifications to your tools

The `fa notify` command was intended to be placed in status bars or shell prompts.

The `fa notify` command will output a `!` without a trailing newline character (`\n`).

Some examples of software that support this behaviour are:

* tmux's status bar
* i3's status bar
* bash prompt
* zsh's prompt
* and many more

## Adding notifications to your Bash prompt

This section provides an example of how you could add the `fa notify`
command in a Bash prompt to check if the current date matches any of
the dates in your agenda.

**Note**: This section assumes you have a `fa` executable in a directory on your `$PATH`.

### To add notifications to your Bash prompt

1. Add the following contents to your `~/.bashrc`:

   ```
   event () {
     fa notify | awk '{ print " " $1 }'
   }

   yellow="\[\e[1;33m\]"
   reset="\[\e[0;39m\]"

   export PS1="\w$yellow\$(event)$reset > "
   ```

2. Open a new terminal.

Here's what it looks like:

![A screenshot of a bash prompt with an exclamation mark to signify that an event is happening on the current date](images/bashrc-screenshot.png)

# Contributors

* [Jesse Laprade](https://m455.casa)
* [Will Sinatra](http://lambdacreate.com)

# Todos

* Test installation instructions for other Linux distributions
* Decide whether or not we should we create separate installation
  instructions based on Linux distributions?
* Decide whether or not we want uninstallation instructions using
  the makefile for non-custom installation locations
* Ask Will about best location for local installations
