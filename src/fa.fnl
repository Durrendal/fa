#!/usr/bin/env fennel

;; For debugging ----------------------;;
;; (local view (require :fennelview))
;; (local inspect (require :inspect))
;; (global pp (fn [x] (print (view x))))
;; ------------------------------------;;

(local lume (require :lume))
(local home (os.getenv "HOME"))

(local program-name "fa")
(local program-file (.. home "/." program-name))

;; Input must respect this format
(local date-fmt "%m%d")

;; Not in use (yet?)
;; This gets lowercased by (get-day)
;; (local day-fmt "%a")

(local required-key-length (length date-fmt))

(local event-symbol "!")

(local help-cmd "help")
(local init-cmd "init")
(local migrate-cmd "migrate")
(local add-cmd "add")
(local rm-cmd "rm")
(local ls-cmd "ls")
(local notify-cmd "notify")
(local today-cmd "today")

(local newline "\n")
(local double-newline "\n\n")
(local space " ")
(local indent-2 "  ")

(local messages
       {:usage
        (string.format "For usage, type '%s %s'." program-name help-cmd)

        :file-already-exists
        (string.format "A file or directory named '%s' already exists." program-file)

        :date-already-exists
        (.. "The following event already exists in your agenda:" newline
            "%s %s" newline
            "Choose one of the options below:" newline
            "1. Replace the old event" newline
            "2. Add the event to the existing date" newline
            "3. Cancell even creation" newline
            "> ")

        :file-doesnt-exist
        (string.format "%s doesn't exist. Try running '%s %s' (without the quotation marks)."
                       program-file
                       program-name
                       init-cmd)

        :cancelling-date-creation
        "Cancelling date creation"

        :not-an-option
        "Error: '%s' is not an option."

        :date-format
        (.. "Error: Date format must be mmdd" newline
            "Example: 1231 \"The last day of december\"")

        :key-doesnt-exist
        "The date '%s' wasn't found in your agenda."

        :sub-key-doesnt-exist
        "The item '%s' on %s wasn't found in your agenda."

        :agenda-empty
        "There is nothing in your agenda."

        :initialized
        (string.format "Successfully created %s" program-file)

        :added
        "Added '%s' to your agenda."

        :removed
        "Removed '%s' from your agenda."

        :migration-not-necessary
        "Default category already exists, migration is not necessary at this time."})

(fn key-exists? [tbl key]
  (let [tbl-keys (lume.keys tbl)]
    (if (lume.any tbl-keys #(= key $1))
        true
        false)))

(fn get-date []
  (os.date date-fmt))

;; Not in use (yet?)
;; (fn get-day []
;;   (string.lower (os.date day-fmt)))

(fn file-exists? [str]
  (let [file-in (io.open str :r)]
    (if file-in
        (file-in:close)
        false)))

(fn file->table [str]
  (if (file-exists? str)
      (with-open [file-in (io.open str :r)]
        (lume.deserialize (file-in:read :*all)))
      nil))

(fn table->file [str tbl]
  (if (file-exists? str)
      (with-open [file-out (io.open str :w)]
        (file-out:write (lume.serialize tbl)))
      nil))

(fn create-prefix [seq key]
  (if (> (# seq) 1)
      (string.format "%s. " key)
      ""))

(fn print-format [str ...]
  (print (string.format str ...)))

(fn init/create-file []
  (with-open [file-out (io.open program-file :w)]
    (file-out:write (lume.serialize []))
    (print messages.initialized)))

(fn init []
  (if (file-exists? program-file)
      (print messages.file-already-exists)
      (init/create-file)))

(fn migrate []
  ;;Migrate old .fa to new category format
  (if (file-exists? program-file)
      (let
          [tbl         (file->table program-file)
           categories? (. tbl "default")
           migration   {"default" {}}]
        (if (= categories? nil)
            (do
              (each [k v (pairs tbl)]
                (table.insert migration.default {k v}))
              (table->file program-file migration))
            (print messages.migration-not-necessary)))
      (print messages.file-doesnt-exist)))      

(fn add/add-date [tbl date-str event-str]
  (if (key-exists? tbl date-str)
      (do (tset tbl date-str (+ 1 (length (. tbl date-str))) event-str)
          (table->file program-file tbl)
          (print-format messages.added event-str))
      (do (tset tbl date-str [event-str])
          (table->file program-file tbl)
          (print-format messages.added event-str))))

(fn add [date-str event-str]
  (let [file-cond  (file-exists? program-file)
        date-cond  (= (length date-str) required-key-length)
        today-cond (= date-str today-cmd)]
    (match [file-cond date-cond today-cond]
      [true _ true] (add (get-date) event-str)
      [true true]   (add/add-date (file->table program-file) date-str event-str)
      [true false]  (print messages.date-format)
      [false]       (print messages.file-doesnt-exist))))

(fn rm [date-str event-str]
  (if (file-exists? program-file)
      (let [tbl          (file->table program-file)
            seqof-events (. tbl date-str)
            event-num    (tonumber event-str)]
            ;; if date-str and event-num
        (if (and (key-exists? tbl date-str)
                 (key-exists? seqof-events event-num))
            (let [event (. seqof-events event-num)]
              (table.remove seqof-events event-num)
              (if (= (# seqof-events) 0)
                  (do (tset tbl date-str nil)
                      (table->file program-file tbl)
                      (print-format messages.removed event))
                  (do (tset tbl date-str seqof-events)
                      (table->file program-file tbl)
                      (print-format messages.removed event))))

            ;; if date-str today
            (= date-str today-cmd)
            (let [current-date (get-date)]
              (rm current-date event-str))

            ;; if date-str is given, but no event-str is given
            (and (key-exists? tbl date-str)
                 (= event-str nil))
            (do (tset tbl date-str nil)
                (table->file program-file tbl)
                (print-format messages.removed date-str))

            ;; if date-str exists but event-num doesn't exist
            (and (key-exists? tbl date-str)
                 (not (key-exists? seqof-events event-num)))
            (print-format messages.sub-key-doesnt-exist event-str date-str)

            ;; if date-str doesn't exist
            (print-format messages.key-doesnt-exist date-str)))
      (print messages.file-doesnt-exist)))

(fn check/print-seq [seqof-events]
  (let [keys (lume.keys seqof-events)]
    ;; Keys are used here instead of the sequence values, so the keys
    ;; print out in order
    (each [_ key (pairs keys)]
      (let [event        (. seqof-events key)
            event-prefix (create-prefix seqof-events key)]
        (print-format "%s%s" event-prefix event)))))

(fn check [show-events?]
  (if (file-exists? program-file)
      (let [tbl          (file->table program-file)
            current-date (get-date)]
        (when (key-exists? tbl current-date)
          (if (not show-events?)
              (io.write event-symbol)
              (let [seqof-events (. tbl current-date)]
                (check/print-seq seqof-events)))))
      (print messages.file-doesnt-exist)))

(fn ls/print-seq [seqof-events]
  (let [[first-event]           seqof-events
        [first-key & rest-keys] (lume.keys seqof-events)
        first-event-prefix      (create-prefix seqof-events first-key)]
    (print-format "%s%s" first-event-prefix first-event)
    ;; The rest-keys is used here instead of a rest-events, so the
    ;; keys for the underlying table in the sequence don't restart at 1
    (each [_ key (pairs rest-keys)]
      (let [event (. seqof-events key)]
        (print-format "      %s. %s" key event)))))

(fn ls/sort-and-print-tbl [tbl tbl-keys]
  (table.sort tbl-keys)
  (each [_ date (pairs tbl-keys)]
    ;; Print the date without trailing newline character
    (io.write (string.format "%s: " date))
    (let [seqof-events (. tbl date)]
      (ls/print-seq seqof-events))))

(fn ls []
  (if (file-exists? program-file)
      (let [tbl        (file->table program-file)
            tbl-keys   (lume.keys tbl)
            tbl-length (# tbl-keys)]
        (if (> tbl-length 0)
            (ls/sort-and-print-tbl tbl tbl-keys)
            (print messages.agenda-empty)))
      (print messages.file-doesnt-exist)))

(fn help []
  (io.write
   (..
    "Usage:" newline
    indent-2 program-name space "<command> [<arg>] [<arg>]"

    double-newline

    "Commands:" newline
    indent-2 init-cmd space "- Creates your agenda." newline
    indent-2 add-cmd space "<mmdd|today>" space "\"Quoted text\"" space "- Adds an event to your agenda." newline
    indent-2 ls-cmd space "- Displays a list of dates and their events from your agenda." newline
    indent-2 notify-cmd space "- Checks if the current date matches a date in your agenda, and if it matches, then a" space "\"" event-symbol "\" will be displayed." newline
    indent-2 today-cmd space "- The same as the" space notify-cmd " command, but the event text will be displayed, instead of a \"" event-symbol "\"." newline
    indent-2 rm-cmd space "<mmdd|today> - Removes a given date from your agenda." newline
    indent-2 rm-cmd space "<mmdd|today> <number> - Removes an event from given date from your agenda."

    double-newline

    "Examples:" newline
    indent-2 program-name space init-cmd newline
    indent-2 program-name space add-cmd space "1231" space "\"Sherry's birthday\"" newline
    indent-2 program-name space add-cmd space today-cmd space "\"Sherry's birthday\"" newline
    indent-2 program-name space ls-cmd newline
    indent-2 program-name space notify-cmd newline
    indent-2 program-name space today-cmd newline
    indent-2 program-name space rm-cmd space "1231" newline
    indent-2 program-name space rm-cmd space "1231" space "3" space "(See note below)" newline
    indent-2 program-name space rm-cmd space today-cmd newline
    indent-2 program-name space rm-cmd space today-cmd space "3" space "(See note below)"

    double-newline

    "Note: You may need to run" space "'" program-name space ls-cmd "'" space "to see which number correlates to which event." newline)))

(fn process-args [arg-tbl]
  (match arg-tbl
    [add-cmd date-str event-str nil] (add date-str event-str)
    [help-cmd nil]                   (help)
    [init-cmd nil]                   (init)
    [migrate-cmd nil]                (migrate)
    [rm-cmd date-str nil]            (rm date-str)
    [rm-cmd date-str event-str nil]  (rm date-str event-str)
    [ls-cmd nil]                     (ls)
    [notify-cmd nil]                 (check)
    [today-cmd nil]                  (check :show-events)
    _                                (print messages.usage)))

(fn main [arg-tbl]
  (process-args arg-tbl))

(main arg)
